document.addEventListener('DOMContentLoaded', function () {
    $('.confirm-user').click(function (e) {
        var objectType = $(e.target).attr('data-type');
        var uid = $(e.target).attr('data-id');
        console.log(objectType, uid);
        $.ajax({
            type: 'POST',
            url: '/confirm-user',
            data: JSON.stringify({
                uid: uid,
                objectType: objectType
            }),
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                if(result.success){
                    $(e.target).html('ok');
                    $(e.target).attr('disabled', 'disabled');
                }
            }
        });
    });
});