// These two lines are required to initialize Express in Cloud Code.
express = require('express');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
var parseExpressCookieSession = require('parse-express-cookie-session');
app = express();

// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(express.bodyParser());    // Middleware for reading request body

// This is an example of hooking up a request handler with a specific request
// path and HTTP verb using the Express routing API.
app.use(express.bodyParser());
app.use(express.json());
app.use(express.cookieParser('QWEASDQWEASD'));
app.use(parseExpressCookieSession({ cookie: { maxAge: 3600000 } }));

// You could have a "Log In" link on your website pointing to this.
function handleParseError(err, res) {
    switch (err.code) {
        case Parse.Error.INVALID_SESSION_TOKEN:
             Parse.User.logOut();
             res.redirect('/login');
             break;
        default :
            res.redirect('/error')
    }
}

// Clicking submit on the login form triggers this.
app.post('/login', function(req, res) {
    console.log(req.body);
    Parse.User.logIn(req.body.username, req.body.password).then(function() {
            // Login succeeded, redirect to homepage.
            // parseExpressCookieSession will automatically set cookie.
            res.redirect('/');
        },
        function(error) {
            // Login failed, redirect back to login form.
            console.log(error);
            res.redirect('/login?message=' + encodeURIComponent('invalid login data'));
        });
});

app.get('/login', function(req, res) {
    // Renders the login form asking for username and password.
    res.render('login');
});

// You could have a "Log Out" link on your website pointing to this.
app.get('/logout', function(req, res) {
    Parse.User.logOut();
    res.redirect('/');
});

// The homepage renders differently depending on whether user is logged in.
app.get('/', function(req, res) {
    var user = Parse.User.current();
    if (user) {
        Parse.User.current().fetch().then(function(user) {
                if(user.get('type') === 'admin'){
                    res.render('index', {userName: user.get('firstname')});
                } else {
                    Parse.User.logOut();
                    res.render('error', {errorMessage: 'You must be administrator'});
                }
            },
            function(error) {
                handleParseError(error);
            });
    } else {
        console.log('/ url says u are not logged in');
        res.redirect('/login');
    }
});

app.get('/users', function (req, res) {
    if (!Parse.User.current()) {
        console.log('Please, log in');
        res.redirect('/login');
        return;

    }

    var users = [];
    var Doc = Parse.Object.extend('Doctor');
    var query = new Parse.Query(Doc);
    query.equalTo('confirmed', false);
    query.find().then(function (result) {
        result.map(function (doc) {
            doc.type = 'Doctor';
            users.push(doc);
        });
    }).then(function (result) {
        var Parent = Parse.Object.extend('Parent');
        var query = new Parse.Query(Parent);
        query.equalTo('confirmed', false);
        query.find().then(function (result) {
            result.map(function (doc) {
                doc.type = 'Parent';
                users.push(doc);
            });
        }).then(function () {
            res.render('users', {users: users});
        });
    });
});

app.post('/confirm-user', function (req, res) {
    var uid = req.body.uid;
    var type = req.body.objectType;
    console.log('USER DATA: ', req);
    console.log(req.body.uid);
    console.log(req.body.objectType);
    var User = Parse.Object.extend(type);
    var query = new Parse.Query(User);
    query.get(uid, {
        success: function (user) {
            user.set('confirmed', true);
            user.save();
            res.send({success: true});
        },
        error: function () {
            res.send({success: false});
        }
    })
});

// Attach the Express app to Cloud Code.
app.listen();